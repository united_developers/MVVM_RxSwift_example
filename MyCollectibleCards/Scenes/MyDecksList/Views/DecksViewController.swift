import UIKit
import RxSwift
import RxCocoa

class DecksViewController: BaseViewController, Instantiable {
    
    // MARK: - IBOutlet
    
    @IBOutlet private weak var decksCollectionView: UICollectionView!
    
    // MARK: - Properties
    
    public var decks = PublishSubject<[Deck]>()
    private let disposeBag = DisposeBag()
    var myDecksViewModel = MyDecksViewModel()
    
    // MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBinding()
    }
    
    // MARK: - Bindings
    private func configureBinding(){
        
        // Binding collection view
        decks.bind(to: decksCollectionView.rx.items(cellIdentifier: "DecksCollectionViewCell", cellType: DecksCollectionViewCell.self)) {  (row,deck,cell) in
            cell.updateItem(deck)
        }.disposed(by: disposeBag)
        
        // Loading cards by clicking on the deck
        decksCollectionView
            .rx
            .modelSelected(Deck.self)
            .subscribe(onNext: { (deck) in
                self.myDecksViewModel.getCardsById(deck.id ?? "")
            }).disposed(by: disposeBag)
        
        // Selected deck animation
        decksCollectionView
            .rx
            .itemSelected
            .subscribe(onNext: { (index) in
                self.decksCollectionView.selectItem(at: index, animated: true, scrollPosition: .top)
            }).disposed(by: disposeBag)
        
        
    }
    
    static var storyboardName: StringConvertible {
        return Constants.StoryBoard.main
    }
}
