import UIKit
import RxSwift
import RxCocoa

class CardsViewController: BaseViewController, Instantiable, UISearchBarDelegate {
    
    // MARK: - IBOutlet
    
    @IBOutlet private weak var cardsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Properties
    
    public var cards = PublishSubject<[Card]>()
    private let disposeBag = DisposeBag()
    var myDecksViewModel = MyDecksViewModel()
    
    // MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        configureBinding()
    }
    
    // MARK: - Bindings
    
    private func configureBinding() {
        
        // Search Binding
        let query = searchBar.rx.text
            .orEmpty
            .distinctUntilChanged()
        
        // Combining search and table view
        Observable.combineLatest(cards, query) { [unowned self] (allCards, query) -> [Card] in
            return myDecksViewModel.filteredCards(with: allCards, query: query)
        }
        .bind(to: cardsTableView.rx.items(cellIdentifier: "MyCardCell", cellType: MyCardCell.self)) {  (row,card,cell) in
            cell.updateItem(card)
        }.disposed(by: disposeBag)
        
        // Cell display animation
        cardsTableView.rx.willDisplayCell
            .subscribe(onNext: ({ (cell,indexPath) in
                cell.alpha = 0.2
                let transform = CATransform3DTranslate(CATransform3DIdentity, -100, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    cell.alpha = 1
                    cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            })).disposed(by: disposeBag)
        
    }
    
    static var storyboardName: StringConvertible {
        return Constants.StoryBoard.main
    }
}

// MARK: - UITableViewDelegate

extension CardsViewController: UITableViewDelegate {
    // Cell size
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
}
