import UIKit
import RxSwift
import RxCocoa

class MyDecksViewController: BaseViewController, Instantiable {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var cardsViewContainer: UIView!
    @IBOutlet weak var cardTitleLabel: UILabel!
    @IBOutlet weak var decksViewContainer: UIView!
    @IBOutlet weak var addCardButton: UIButton!
    
    // MARK: - Properties
    
    // Lazy initialization of a controller with a list of decks
    private lazy var decksViewController: DecksViewController = {
        
        let viewController = DecksViewController.instantiateFromStoryboard()
        viewController.myDecksViewModel = myDecksViewModel
        // Add View Controller as Child View Controller
        self.addChildToContainer(childViewController: viewController, to: decksViewContainer)
        
        return viewController
        
    }()
    
    // Lazy initialization of a controller with a list of cards
    private lazy var cardsViewController: CardsViewController = {
        
        let viewController = CardsViewController.instantiateFromStoryboard()
        viewController.myDecksViewModel = myDecksViewModel
        // Add View Controller as Child View Controller
        self.addChildToContainer(childViewController: viewController, to: cardsViewContainer)
        
        return viewController
    }()
    
    // ViewModel
    lazy var myDecksViewModel: MyDecksViewModel = {
        return MyDecksViewModel()
    }()
    
    let disposeBag = DisposeBag()
    
    // MARK: - View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBinding()
        bindViewControllerContainer()
        bindAddCardButton()
        myDecksViewModel.getMyDecks()
    }
    
    
    // MARK: - Bindings
    
    private func configureBinding() {
        
        // Show or hide UI depending on deck is selected or not
        myDecksViewModel.isDeckSelected
            .subscribe(onNext: { [weak self] isDeckSelected in
                self?.configureCardsUI(isDeckSelected)
            })
            .disposed(by: disposeBag)
        
        // Binding loading
        myDecksViewModel.loadingDecks
            .subscribe(onNext: { [weak self] loading in
                if let decksView = self?.decksViewContainer {
                    self?.configureLoadingInsideView(loading, view: decksView)
                }
            })
            .disposed(by: disposeBag)
        
        // Display an error alert if an error was received
        myDecksViewModel.errorMessage.drive(onNext: { (_message) in
            if let message = _message {
                self.showErrorAlert(message: message)
            }
        }).disposed(by: disposeBag)
        
    }
    
    private func bindViewControllerContainer() {
        
        // Binding decks to deck container
        
        myDecksViewModel
            .decks
            .observe(on: MainScheduler.instance)
            .bind(to: decksViewController.decks)
            .disposed(by: disposeBag)
        
        // Binding cards to card container
        
        myDecksViewModel
            .cards
            .observe(on: MainScheduler.instance)
            .bind(to: cardsViewController.cards)
            .disposed(by: disposeBag)
    }
    
    // Button binding
    private func bindAddCardButton() {
        self.addCardButton.rx.tap
            .subscribe { [weak self] _ in
                
                guard let self = self else { return }
                
                let addCardViewController = AddCardViewController.instantiateFromStoryboard()
                
                // Subscription to change the card
                addCardViewController.addCardViewModel.card.subscribe(onNext :{ [weak self] card in
                    guard let self = self else { return }
                    self.myDecksViewModel.addNewCard(card)
                    addCardViewController.dismiss(animated: true, completion: nil)
                }).disposed(by: self.disposeBag)
                
                // Show the add card view controller
                self.present(addCardViewController, animated: true, completion: nil)
                
            }.disposed(by: disposeBag)
    }
    
    // Show or hide UI depending on deck is selected or not
    func configureCardsUI(_ isDeckSelected: Bool) {
        cardsViewContainer.isHidden = !isDeckSelected
        cardTitleLabel.isHidden = !isDeckSelected
        addCardButton.isHidden = !isDeckSelected
    }
    
    static var storyboardName: StringConvertible {
        return Constants.StoryBoard.main
    }
    
}





