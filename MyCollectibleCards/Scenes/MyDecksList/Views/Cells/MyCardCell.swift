import Foundation
import Alamofire
import AlamofireImage

import UIKit

class MyCardCell: BaseTableCell {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var nameCardLabel: UILabel!
    @IBOutlet weak var rarityCardLabel: UILabel!
    @IBOutlet weak var manaValueLabel: UILabel!
    
    // Initializing a cell using data
    override func updateItem(_ data: Any) {
        if let card = data as? Card {
            nameCardLabel.text = card.name
            rarityCardLabel.text = card.rarity?.rawValue
            manaValueLabel.text = String(describing: card.manaValue ?? 0)
            cardImageView.loadImage(url: card.imageUrl ?? Constants.defaultCardUrl)
        }
    }
    
}
