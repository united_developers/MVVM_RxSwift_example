import UIKit

class DecksCollectionViewCell: BaseCollectionCell {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var deckImage: UIImageView!
    @IBOutlet weak var deckNameLabel: UILabel!
    
    // Cell animation depending on selected or not
    override var isSelected: Bool {
        didSet {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                self.layer.zPosition = self.isSelected ? 1 : -1
                self.transform = self.isSelected ? CGAffineTransform(scaleX: 1.2, y: 1.2) : CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
    // Initializing a cell using data
    override func updateItem(_ data: Any) {
        if let deck = data as? Deck {
            self.deckImage.loadImage(url: deck.imageDeck ?? Constants.defaultCardUrl)
            self.deckNameLabel.text = deck.name
        }
    }
    
}
