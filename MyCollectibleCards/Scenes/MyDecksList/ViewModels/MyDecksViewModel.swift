import Foundation
import RxSwift
import RxCocoa

class MyDecksViewModel {
    
    // MARK: - Properties
    
    public let decks : PublishSubject<[Deck]> = PublishSubject()
    public var cards : BehaviorRelay<[Card]> = BehaviorRelay(value: [])
    public let isDeckSelected: PublishSubject<Bool> = PublishSubject()
    public let loadingDecks: PublishSubject<Bool> = PublishSubject()
    private let _error = BehaviorRelay<Bool>(value: false)
    private let _errorMessage = BehaviorRelay<String?>(value: nil)
    let apiService: AlamofireClientProtocol
    
    // Error state
    var hasError: Bool {
        return _error.value
    }
    
    var errorMessage: Driver<String?> {
        return _errorMessage.asDriver()
    }
    
    private let disposable = DisposeBag()
    
    /// Initializer
    /// - parameter apiService: Protocol for working with networking
    init( apiService: AlamofireClientProtocol = AlamofireClient()) {
        self.apiService = apiService
    }
    
    // MARK: - Functions
    
    // Get a list of decks
    func getMyDecks() {
        self.isDeckSelected.onNext(false)
        self.loadingDecks.onNext(true)
        apiService.getMyDecks { result in
            self.loadingDecks.onNext(false)
            switch result {
            case .success(let decksResponse):
                self._error.accept(false)
                self._errorMessage.accept(nil)
                let decks = decksResponse 
                self.decks.onNext(decks)
            case .failure(let error):
                // Set error
                self.setError(error.localizedDescription)
                print(error.localizedDescription)
            }
        }
    }
    
    /// Get a list of cards for the selected deck
    /// - parameter deckId: Selected deck ID
    func getCardsById(_ deckId: String) {
        apiService.getCardsById(deckId: deckId, completion: { result in
            switch result {
            case .success(let cardsResponse):
                self._error.accept(false)
                self._errorMessage.accept(nil)
                self.isDeckSelected.onNext(true)
                let cards = cardsResponse
                self.cards.accept(cards)
            case .failure(let error):
                // Set error
                self.setError(error.localizedDescription)
                print(error.localizedDescription)
            }
        })
    }
    
    /// Filtering cards by text from search
    /// - parameter allCards: List all card
    /// - parameter query: Search text
    func filteredCards(with allCards: [Card], query: String) -> [Card] {
        guard !query.isEmpty else { return allCards }
        
        let filteredCards = allCards.filter { $0.name?.lowercased().contains(query.lowercased()) ?? false }
        
        return filteredCards
    }
    
    // Add new card
    func addNewCard(_ newCard: Card) {
        cards.accept(cards.value + [newCard])
    }
    
    // Set error
    func setError(_ message: String) {
        self._error.accept(true)
        self._errorMessage.accept(message)
    }
}
