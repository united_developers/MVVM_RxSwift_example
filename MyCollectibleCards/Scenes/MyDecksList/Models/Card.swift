import Foundation

struct Card : Codable {
    
    var imageUrl: String?
    var name: String?
    var rarity: Rarity?
    var manaValue: Int?
    
    /// Initializer
    /// - parameter imageUrl: Link to image with card
    /// - parameter name: Card name
    /// - parameter rarity: Enum with a list of rarity types
    /// - parameter manaValue: Int value for specifying mana value
    init(imageUrl: String,name: String,  rarity: Rarity, manaValue: Int) {
        self.imageUrl = imageUrl
        self.name = name
        self.rarity = rarity
        self.manaValue = manaValue
    }
    
    // Coding keys
    enum CodingKeys: String, CodingKey {
        case imageUrl = "imageURL"
        case name = "name"
        case rarity = "rarity"
        case manaValue = "manaValue"
    }
    
    // Decoding using keys
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        rarity = try values.decodeIfPresent(Rarity.self, forKey: .rarity)
        manaValue = try values.decodeIfPresent(Int.self, forKey: .manaValue)
    }
    
}
