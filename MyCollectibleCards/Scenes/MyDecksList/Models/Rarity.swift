import Foundation

enum Rarity: String {
    // Rarity types
    case  common = "Common"
    case uncommon = "Uncommon"
    case rare = "Rare"
    case mythicRare = "Mythic Rare"
    case unknown = "Unknow"
    
    static let allRarityType = [common.rawValue, uncommon.rawValue, rare.rawValue, mythicRare.rawValue, unknown.rawValue]
}

// Extension for decoding in enum
extension Rarity: Codable {
    public init(from decoder: Decoder) throws {
        self = try Rarity(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? .unknown
    }
}
