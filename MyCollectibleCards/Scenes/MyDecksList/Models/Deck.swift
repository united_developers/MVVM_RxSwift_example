import Foundation

struct Deck : Codable {
    
    var id : String?
    var imageDeck: String?
    var name: String?
    
    /// Initializer
    /// - parameter id: Identifier deck
    /// - parameter imageDeck: Сard deck cover link
    /// - parameter name: Deck name
    init(id: String, imageDeck: String, name: String) {
        self.id = id
        self.imageDeck = imageDeck
        self.name = name
    }
    
    // Coding keys
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case imageDeck = "imageDeck"
        case name = "name"
    }
    
    // Decoding using keys
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        imageDeck = try values.decodeIfPresent(String.self, forKey: .imageDeck)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
}
