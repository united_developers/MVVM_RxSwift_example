import UIKit
import RxSwift
import RxCocoa

class AddCardViewController: BaseViewController, Instantiable, UIPickerViewDelegate {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var rarityTextField: UITextField!
    @IBOutlet weak var manaValueTextField: UITextField!
    @IBOutlet weak var addCardButton: UIButton!
    
    // MARK: - Properties
    
    var addCardViewModel = AddCardViewModel()
    let disposeBag = DisposeBag()
    
    // MARK: - View Cycle
    
    override func viewDidLoad() {
        bindAddCardButton()
        bindRarityPickerView()
    }
    
    // MARK: - Bindings
    
    // Button binding
    private func bindAddCardButton() {
        addCardButton.rx.tap
            .bind {
                self.onTapAddCard()
            }
            .disposed(by: disposeBag)
    }
    
    // Picker binding to select the type of rarity
    private func bindRarityPickerView() {
        let pickerView = UIPickerView()
        rarityTextField.inputView = pickerView
        
        addCardViewModel.allRarityType.bind(to: pickerView.rx.itemTitles) { (row, element) in
            return element
        }
        .disposed(by: disposeBag)
        
        pickerView.rx.itemSelected
            .subscribe { (event) in
                switch event {
                case .next(let selected):
                    self.rarityTextField.text = self.addCardViewModel.allRarityType.value[selected.row]
                default:
                    break
                }
            }
            .disposed(by: disposeBag)
    }
    
    // Handling clicking on the add card button
    private func onTapAddCard() {
        guard let name = nameTextField.text, let rarityText = rarityTextField.text, let rarity = Rarity(rawValue: rarityText) , let manaValueText = manaValueTextField.text, let manaValue = Int(manaValueText) else {return}
        addCardViewModel.addCard(name: name, rarity: rarity, manaValue: manaValue)
    }
    
    static var storyboardName: StringConvertible {
        return Constants.StoryBoard.main
    }
}
