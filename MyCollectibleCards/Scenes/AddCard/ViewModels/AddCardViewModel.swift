import Foundation
import RxSwift
import RxCocoa

class AddCardViewModel {
    
    // MARK: - Properties
    
    let card = PublishSubject<Card>()
    let allRarityType : BehaviorRelay<[String]> = BehaviorRelay(value: Rarity.allRarityType)
    
    // MARK: - Functions
    
    /// Method for adding a new card
    /// - parameter name: Card name
    /// - parameter rarity: Enum with a list of rarity types
    /// - parameter manaValue: Int value for specifying mana value
    func addCard(name: String, rarity: Rarity, manaValue: Int) {
        let newCard = Card(imageUrl: Constants.defaultCardUrl, name: name, rarity: rarity, manaValue: manaValue)
        card.onNext(newCard)
    }
}

