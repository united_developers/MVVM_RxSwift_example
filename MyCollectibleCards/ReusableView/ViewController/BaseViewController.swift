import UIKit

class BaseViewController: UIViewController {
    
    // Displaying an alert with an error
    func showErrorAlert(message: String, completion: ((UIAlertAction) -> Void)? =  nil) {
        let errorAlertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let actionDone = UIAlertAction(title: "Done", style: .default, handler: completion)
        errorAlertController.addAction(actionDone)
        present(errorAlertController, animated: true, completion: nil)
    }
    
    // Show or hide a loader in a container
    func configureLoadingInsideView(_ isLoading: Bool, view: UIView) {
        if isLoading {
            view.displayAnimatedActivityIndicatorView()
        } else {
            view.hideAnimatedActivityIndicatorView()
        }
    }
}
