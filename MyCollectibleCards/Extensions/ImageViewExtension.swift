import UIKit
import Alamofire
import AlamofireImage

extension UIImageView {
    
    func loadImage(url: String) {
        guard let imageURL = URL(string: url) else {
            return
        }
        
        AF.request(imageURL).responseImage { response in
            
            switch response.result {
            case .success(let image):
                self.image = image
            case .failure:
                // Using the default image if an error occurs
                self.image = UIImage(named: "defaultCardImage")
            }
            
        }
        
    }
    
}
