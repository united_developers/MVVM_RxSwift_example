import Alamofire
import Foundation

enum AlamofireRouter: URLRequestConvertible {
    
    // MARK: - List method
    
    case getMyDecksList
    case getCardsById(deckId: String)
    
    // MARK: - HTTPMethod
    
    private var method: HTTPMethod {
        switch self {
        case .getMyDecksList:
            return .get
        case .getCardsById:
            return .get
        }
    }
    
    // MARK: - Path
    
    private var path: String {
        switch self {
        case .getMyDecksList:
            return "/myDecks"
        case .getCardsById(let deckId):
            return "/getCardsById/\(deckId)"
        }
    }
    
    // MARK: - Parameters
    
    private var parameters: Parameters? {
        switch self {
        case .getMyDecksList:
            return nil
        case .getCardsById:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url =  Constants.API.baseURL
        
        var request = URLRequest(url:  URL(string: url.appending(path))!)
        
        request.addValue(Constants.API.defaultContentType, forHTTPHeaderField: Constants.API.contentType)
        request.httpMethod = method.rawValue

        if let parameters = parameters {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return request
    }
    
}
