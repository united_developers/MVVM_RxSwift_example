import Alamofire

class AlamofireClient: AlamofireClientProtocol {
    
    /// Network request
    /// - parameter T: generic object of Decodable type
    /// - parameter route: request object
    /// - parameter completion: to receive callback
    @discardableResult
    private static func performRequest<T:Decodable>(route:AlamofireRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T, AFError>)->Void) -> DataRequest {
        return AF.request(route)
            .responseDecodable (decoder: decoder){ (response: DataResponse<T, AFError>) in
                completion(response.result)
            }
    }
    
    /// Getting a list of decks
    /// - parameter completion: to receive callback
    func getMyDecks(completion:@escaping (Result<[Deck], AFError>)->Void) {
        AlamofireClient.performRequest(route: AlamofireRouter.getMyDecksList, completion: completion)
    }
    
    /// Getting cards by deck ID
    /// - parameter deckId: identifier deck
    /// - parameter completion: to receive callback
    func getCardsById(deckId: String, completion:@escaping (Result<[Card], AFError>)->Void) {
        AlamofireClient.performRequest(route: AlamofireRouter.getCardsById(deckId: deckId), completion: completion)
    }
}

protocol AlamofireClientProtocol {
    func getMyDecks(completion:@escaping (Result<[Deck], AFError>)->Void)
    func getCardsById(deckId: String, completion:@escaping (Result<[Card], AFError>)->Void)
}
