import Foundation

struct Constants {
    // Constants for working with networking
    struct API {
        static let baseURL = "https://private-07889-mycollectiblecards.apiary-mock.com"
        static let contentType = "Content-Type"
        static let defaultContentType = "application/json"
    }
    // Constants for working with Storyboards
    struct StoryBoard {
        static let main = "Main"
    }
    // Unit Test Constants
    struct UnitTest {
        static let idDeck = "deck2asdfghjkl"
        static let countDecks = 3
        static let countCards = 8
    }
    // Default image in case an error occurs, and etc.
    static let defaultCardUrl = "https://www.giantbomb.com/a/uploads/scale_small/8/88760/2270006-mtgbackcard.png"
}
