import XCTest
import Alamofire
import RxSwift
import RxTest
@testable import MyCollectibleCards

class MyDecksViewModelTests: XCTestCase {
    
    var model: MyDecksViewModel!
    var mockAPIService: MockApiService!
    var scheduler: TestScheduler!
    var result: TestableObserver<String>!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        scheduler = TestScheduler(initialClock: 0)
        mockAPIService = MockApiService()
        model = MyDecksViewModel(apiService: mockAPIService)
        result = scheduler.createObserver(String.self)
        disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        model = nil
        mockAPIService = nil
        super.tearDown()
    }
    
    func test_get_decks() {
        mockAPIService.completeDecks = [Deck]()
        
        model.getMyDecks()
        
        XCTAssert(mockAPIService!.isGetMyDecksCalled)
    }
    
    func test_get_decks_fail() {
        let error = AFError.explicitlyCancelled
        
        model.getMyDecks()
        
        mockAPIService.getMyDecksFail(error: error)
        
        XCTAssertEqual( model.hasError, true )
    }
    
    func test_get_cards() {
        mockAPIService.completeCards = [Card]()
        
        model.getCardsById(Constants.UnitTest.idDeck)
        
        XCTAssert(mockAPIService!.isCardsByIdCalled)
    }
    
    func test_get_cards_fail() {
        let error = AFError.explicitlyCancelled
        
        model.getCardsById(Constants.UnitTest.idDeck)
        
        mockAPIService.getCardsByIdFail(error: error)
        
        XCTAssertEqual( model.hasError, true )
    }
    
    func test_user_selected_deck() {
        let recorder = Recorder<Bool>()

        recorder.on(valueSubject: model.isDeckSelected)
        userSelectedDeck()
        
        // Check if a deck is selected
        let containsSelectedBool = recorder.items.contains(true)
        let countCards = model.cards.value.count
        XCTAssert(containsSelectedBool)
        XCTAssert(countCards > 0)
        
    }
    
    func test_search() {
        userSelectedDeck()
        let input = scheduler.createColdObservable([
            .next(1000, Optional.some("")),
            .next(2000, Optional.some("D")),
            .next(3000, Optional.some("Dra")),
            .next(4000, Optional.some("Dragon")),
        ])
        
        input
            .subscribe(onNext: { query in
                guard let query = query else { return }
                let filteredCards = self.model.filteredCards(with: self.model.cards.value, query: query)
                self.model.cards.accept(filteredCards)
            })
            .disposed(by: disposeBag)
        
        
        scheduler.start()
        XCTAssertEqual( model.cards.value.count, 1)
    }
    
    func test_add_new_card() {
        userSelectedDeck()
        let newCard = Card(imageUrl: Constants.defaultCardUrl, name: "Test new card", rarity: .common, manaValue: 5)
        model.addNewCard(newCard)
        XCTAssertEqual( model.cards.value.count, Constants.UnitTest.countCards + 1)
    }
}

extension MyDecksViewModelTests {
    
    // Simulation of the fact that the user has selected a deck
    private func userSelectedDeck() {
        mockAPIService.completeDecks = MockData().mockDecks()
        model.getMyDecks()
        mockAPIService.getMyDecksSuccess()
        mockAPIService.completeCards = MockData().mockCards()
        model.getCardsById(Constants.UnitTest.idDeck)
        mockAPIService.getCardsByIdSuccess()
    }
    
}

// Mock API service for testing
class MockApiService: AlamofireClientProtocol {
    
    var isGetMyDecksCalled = false
    var isCardsByIdCalled = false
    var completeMyDecks: ((Result<[Deck], AFError>) -> Void)!
    var completeCardsById: ((Result<[Card], AFError>) -> Void)!
    
    var completeDecks: [Deck] = [Deck]()
    var completeCards: [Card] = [Card]()
    
    func getMyDecks(completion: @escaping (Result<[Deck], AFError>) -> Void) {
        isGetMyDecksCalled = true
        completeMyDecks = completion
    }
    
    func getCardsById(deckId: String, completion: @escaping (Result<[Card], AFError>) -> Void) {
        isCardsByIdCalled = true
        completeCardsById = completion
    }
    
    func getMyDecksSuccess() {
        let result = Result<[Deck], AFError>.success(completeDecks)
        completeMyDecks(result)
    }
    
    func getMyDecksFail(error: AFError) {
        let result = Result<[Deck], AFError>.failure(error)
        completeMyDecks(result)
    }
    
    func getCardsByIdSuccess() {
        let result = Result<[Card], AFError>.success(completeCards)
        completeCardsById(result)
    }
    
    func getCardsByIdFail(error: AFError) {
        let result = Result<[Card], AFError>.failure(error)
        completeCardsById(result)
    }
    
}

// Mock data from json files this path /Resources
class MockData {
    
    func mockDecks() -> [Deck] {
        let path = Bundle.main.path(forResource: "MyDecks", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let decoder = JSONDecoder()
        let decks = try! decoder.decode([Deck].self, from: data)
        return decks
    }
    
    func mockCards() -> [Card] {
        let path = Bundle.main.path(forResource: "MyCards", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let decoder = JSONDecoder()
        let cards = try! decoder.decode([Card].self, from: data)
        return cards
    }
    
}

// Storage of events for testing PublishSubject
class Recorder<T> {
    var items = [T]()
    let bag = DisposeBag()
    
    func on(arraySubject: PublishSubject<[T]>) {
        arraySubject.subscribe(onNext: { value in
            self.items = value
        }).disposed(by: bag)
    }
    
    func on(valueSubject: PublishSubject<T>) {
        valueSubject.subscribe(onNext: { value in
            self.items.append(value)
        }).disposed(by: bag)
    }
}
