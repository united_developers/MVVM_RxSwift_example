import XCTest
@testable import MyCollectibleCards

class AlamofireClientTests: XCTestCase {
    
    var service: AlamofireClient?
    
    override func setUp() {
        super.setUp()
        service = AlamofireClient()
    }
    
    override func tearDown() {
        service = nil
        super.tearDown()
    }

    func test_get_decks() {
        
        let service = self.service!
        
        let expect = XCTestExpectation(description: "callback")
        
        service.getMyDecks { result in
            expect.fulfill()
            switch result {
            case .success(let decksResponse):
                let decks = decksResponse
                XCTAssertEqual( decks.count, Constants.UnitTest.countDecks)
                for deck in decks {
                    XCTAssertNotNil(deck.id)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        wait(for: [expect], timeout: 4)
    }
    
    func test_get_cards() {
        
        let service = self.service!
        
        let expect = XCTestExpectation(description: "callback")
        
        service.getCardsById(deckId: Constants.UnitTest.idDeck, completion: { result in
            expect.fulfill()
            switch result {
            case .success(let cardsResponse):
                let cards = cardsResponse
                XCTAssertEqual( cards.count, Constants.UnitTest.countCards)
                for card in cards {
                    XCTAssertNotNil(card.name)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
        wait(for: [expect], timeout: 4)
    }
    
    
}
