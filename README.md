# MVVM with RxSwift

MyCollectibleCards is an example application using RxSwift technology and MVVM architecture and also using Unit tests. 

[![Language](https://img.shields.io/badge/language-Swift%205.0-orange.svg)](https://swift.org)

## Sample App

<img src="./READMEResources/gif_for_repository.gif" width="51%">

